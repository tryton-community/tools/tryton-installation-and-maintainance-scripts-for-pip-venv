#! /bin/bash

clear

# SKRIPT ZUM KOMPLETTEN LÖSCHEN EINER INSTALLATION VON TRYTON

echo "
SKRIPT ZUM KOMPLETTEN LÖSCHEN EINER INSTALLATION VON TRYTON.
"
Benutzer=$(whoami)
instskrverz=$(pwd)

### Test auf sudo-Rechte ###

if [ -z "$sudo_getestet" ] # -z: vorhanden? - für Variablen
then
sudo -v
if [ $? -eq 0 ]
then
  echo "export sudo_getestet=ok" > eingabe.conf
else
echo "
Bitte verschaffen Sie (auf einem anderen Terminal) dem Benutzer $Benutzer sudo-Rechte mit dem Befehl
sudo usermod -aG sudo $Benutzer
(Weiter mit Eingabetaste.)
"
read
fi # $? -eq 0
fi # -z "$sudo_getestet"

# set -e # Ende nach 1.Fehler
# set -x # Fehlermeldungen ausgeben

instskrverz=$(pwd) # instskrverz ist das Verzeichnis, in denen das Installationsskript liegt
datum=`date +%d.%m.%Y`

### Hauptpfad für Tryton-Instanzen ###

if [ -z "$Pfad_TRYTON_Verz" ]
then

echo "
Wir gehen davon aus, daß die virtuellen Umgebungen für Ihrer Tryton-Installation im Verzeichnis ~/TRYTON liegen. In diesem Fall: Eingabetaste.

Sofern Sie einen anderen Pfad in Ihrem Heimatverzeichnis liegen, geben Sie diesen bitte OHNE "/home/ihr-Benutzername/" ein.
"
read Name_TRYTON_Verz
fi

if [ "$Name_TRYTON_Verz" = "" ]
then
Name_TRYTON_Verz=TRYTON
fi
  if ! [ -d "$HOME/$Name_TRYTON_Verz" ] # -d: Prüfung auf Verzeichnis vorhanden
  then
    mkdir $HOME/$Name_TRYTON_Verz
    if [ $? -eq 0 ]
    then
      echo "$HOME/$Name_TRYTON_Verz angelegt."
    else
      exit 1
    fi
  fi
Pfad_TRYTON_Verz=$HOME/$Name_TRYTON_Verz


echo "Hauptpfad für Tryton: $Pfad_TRYTON_Verz"
# cd $Pfad_TRYTON_Verz

### Verzeichnis und Datenbank der Instanz festlegen ###

if [ -z "$neues_Verzeichnis" ] || [ -z "$neue_db_Version" ] # || "oder"-Abfrage
then
echo "
Haben Verzeichnis und Datenbank den gleichen Namen ? "
while true
do
  read -p "(j/n): " gl_Name # Ausgabej/n; frage gl_Name ab
  if [ "$gl_Name" = "j" ] || [ "$gl_Name" = "n" ] # überprüfe, ob j oder n eingegeben
  then
    break # Sprung hinter "done"
  fi
done
fi

if [ "$gl_Name" = "n" ]
then
echo "
Wie heißt das Verzeichnis, das gelöscht werden soll ?
"
read neues_Verzeichnis

echo "
Wie heißt die Datenbank, das gelöscht werden soll ?
"
read neue_db_Version

else

echo "
Wie heißt das Verzeichnis und die Datenbank, die gelöscht werden sollen ? 
"
read neues_Verzeichnis
neue_db_Version=$neues_Verzeichnis

fi # [ "$gl_Name" = "n" ]

### Verzeichnis löschen ###

if [ -d "$Pfad_TRYTON_Verz/$neues_Verzeichnis" ]
then
  rm -r $Pfad_TRYTON_Verz/$neues_Verzeichnis
  if [ $? -eq 0 ]
  then
    echo "$Pfad_TRYTON_Verz/$neues_Verzeichnis gelöscht."
  else
    echo "Löschen des Verzeichnisses $Pfad_TRYTON_Verz/$neues_Verzeichnis ist gescheitert. Bitte auf anderem Terminal korrigieren."
  read
  fi
fi

### ---

### Datenbank-Benutzer und Datenbank-Kennwort abfragen ###

if [ -z "$db_Benutzer" ] || [ -z "$kw_dbBenutzer" ]
then
echo "
Sind der System- und der Datenbank-Benutzer identisch  ?
"
while true
do
  read -p "(j/n): " janein
  if [ "$janein" = "j" ] || [ "$janein" = "n" ]
  then
    break
  fi
done

if [ "$janein" = "j" ]
then
db_Benutzer=$Benutzer

else
echo "
Bitte geben Sie den Namen des Datenbank-Benutzers ein:"
read db_Benutzer
fi
echo "
Bitte geben Sie das Kennwort für Ihren Datenbank-Benutzer $db_Benutzer ein:"
read kw_dbBenutzer

fi # [ -z "$db_Benutzer" ] || [ -z "$kw_dbBenutzer" ]

echo "Datenbank-Benutzer: $db_Benutzer"


## Auf systemweite Tryton- oder mds-Komponenten überprüfen 

pip list | grep -E 'tryton*|mds' > /dev/null
if [[ $? -eq 0 ]]
then
echo "
In Ihrem System sind evt. systemweit Tryton- oder MDS-Module installiert.
Bitte entfernen Sie diese mit 
pip uninstall <Modulname>.
" 
read
fi
###

## Laufende Tryton-Instanzen beenden

# (auf Mac OS X überspringen)
if ! [ "$system" = "Darwin" ]
then

echo "
Sollen aktive Tryton-Instanzen beendet werden ? (nicht auf Servern)
(ja=Eingabe)
"
read janein

if [ "$janein" = "" ] || [ "$janein" = "j" ]
then
killall trytond trytond-cron
# sudo systemctl restart postgresql* 
fi 
##
fi # ! [ "$system" = "Darwin" ]

## Prüfen, ob Instanz als Systemservice läuft
if [ -a /etc/systemd/system/$neues_Verzeichnis.service ]
then
sudo systemctl stop $neues_Verzeichnis.service
fi



### prüfen, ob DaBa noch vorhanden ist:

psql -U $db_Benutzer -l | grep $neues_Verzeichnis > /dev/null
if [ $? -eq 0 ]
then
sudo --login -u postgres dropdb $neue_db_Version
else echo "
Die Datenbank ist nicht mehr vorhanden
"  
fi


echo "
Wenn Sie Tryton als Server (egal ob im lokalen Netz oder entfernt) installiert ist, drücken Sie bitte jetzt 'j'
"
read janein
if ! [ "$janein" = "j" ]; then

echo "
Ende des Skripts. Rückmeldung bitte an wd@trolink.de.
"
exit 0
fi

# Abschnitt Webserver löschen

sudo systemctl disable $neues_Verzeichnis.service

sudo rm /etc/systemd/system/$neues_Verzeichnis.service

### Nginx deaktivieren ###

sudo rm /etc/nginx/sites-available/$neues_Verzeichnis
sudo rm /etc/nginx/sites-enabled/$neues_Verzeichnis


echo "
Ende des Skripts. Rückmeldung bitte an wd@trolink.de.
"
