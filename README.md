# Tryton installation and maintainance scripts for PIP

A little collection of scripts to install and maintain the fabulous "Tryton" ERP system server in python virtual environments. Supported OSes:

+ Ubuntu, Debian
+ Manjaro Linux (probably Arch)
+ Mac OS X - [MacPorts](https://macports.org) required.

# Tryton ERM installation and maintainance scripts for PIP

I need to ask for apologies first. This is my first ever attempt in scripting, so I guess these are the worst scripts you've ever seen. Nevertheless, they proved to be useful in daily life with the incredible FOSS TRYTON ERM/ERP system, which is a full-featured ERP system FOR FREE. For details see tryton-dach.org and tryton.org.

These scripts have been developed with a very German background, here comes another request for apologies for that. There should be English and Spanish versions at least, but at present that's beyound my abilities and capacities. So in doubt try deepl.com.

Certainly, there are lots of mistakes in my code, but I think it's rather unlikely they will blow up your system. PLEASE, flood me with issues, I'll do my very best to iron out maior flaws. Certainly you're well aware that I cannot cover any case which may occur in an installation process, but I'll do my best.

PLEASE MAKE SURE TO ALWAYS HAVE ALL SCRIPTS AND OTHER FILES IN YOUR DIRECTORY, THE MAY DEPEND ON EACH OTHER.

# List of scripts:
## Installations-Skript-Tryton.sh
This is for a PIP installation of tryton in a virtual environment from scratch. You can do installs from Tryton standard repo via PIP and private repos. There are three ways to operate it:

* without module list and database template. This way, only a minimal module set (ir & res) will get installed. Adding more modules, esp. your preferred table of accounts, is recommended.
* with a module list. This way, you can define your selection of modules.
* with a module list and a database template. This way, much of basic setup such as business years, sequences, sample entries for parties, products and a lot more is already done. Be aware that at installation time, module list and templates must match. But it's easy to install more modules after the first run.'

## Update-klein-Tryton.sh
This does a small update within a x.y.* range, such as 5.6.2 > 5.6.[latest].

## Update-groß-Tryton.sh
This does a "big" update x.y.* to x.y+2.*, such as 5.6.2 > 5.8.[latest].
In theory, you can do more than "+2", in reality only the experienced users should try this - and those won't use these scripts anyway... (-;

## Datenbank-importieren.sh
This imports a given Database into a setup already existing. It does not care for differences in setup, esp. module sets. I'm using it to keep my experimental setup synced with my productive (vice versa in case).

## Tryton-starten.sh
Script to launch tryton server/daemon and installed client at a time.

## Tryton-Sicherungsskript.sh
This one is designed to be launched automatically at system boot or regularly by cron. It does a psql dump of the database and checks it it's a duplicate to the present one. If so, the new file will be removed, if not, the old file will be zipped. Such it takes care that no duplicates waste disk space.

## Transaktionen-löschen.sh
This script will DELETE ALL YOUR TRANSACTIONS, such as sales, purchases, deliveries and posts. It tries to preserve all kinds of settings, rules etc.
SO HANDLE WITH CARE, always have a backup before usage.

# Credits
My special thanks and respect go to Eddy Boer from Netherland, who did an incredible job to guide me with unlimited patience through the very basics of installing Tryton with PIP, Dave Harper earns lots of thanks as well. And Michael Steinfatt did all the work for OS X integration. 
