#!/bin/bash

echo "
Dieses Skript sichert eine Tryton-Datenbank in eine Datei im VENV-Verzeichnis.
" 

datum=`date +%H.%M--%d.%m.%Y`
cd ~

echo "
Wir gehen davon aus, daß die virtuellen Umgebungen für Ihre Tryton-Versionen 
unter '~/TRYTON' liegen sollen oder bereits liegen. In diesem Fall: Eingabetaste.

Sofern Sie einen anderen Pfad in Ihrem Heimatverzeichnis wünschen, 
geben Sie diesen bitte OHNE "/home/ihr-Benutzername/" ein. (Eingabe)
"
read Pfadkennung
if [ "$Pfadkennung" = "" ]
then
cd ~/TRYTON
pfadtry=~/TRYTON
else
cd ~/$Pfadkennung
pfadtry=~/$Pfadkennung
fi

echo "
Wie heißt das Verzeichnis der virtuellen Umgebung, 
die zu der zu aktualisierenden Datenbank gehört?"
read neues_Verzeichnis
neue_db_Version=$neues_Verzeichnis

cd $pfadtry/$neues_Verzeichnis

mkdir Sikop

cd Sikop

echo "
Bitte geben sie den Namen des Datenbank-Benutzers ein:"
read db_Benutzer


echo "
Möchten Sie die Option --no-owner setzen ?
(j/n)
"
read janein

if [ "janein" = "n" ]
then
sudo -u postgres -- pg_dump -U postgres $neues_Verzeichnis > Sikop-DB--$neues_Verzeichnis-$datum.sql
# PGPASSWORD=$db_Kennwort
else
sudo -u postgres -- pg_dump --no-owner -U postgres $neues_Verzeichnis > Sikop-DB--$neues_Verzeichnis-$datum.sql
fi

## Kommentar erzeugen ##
echo "
Sofern Sie einen Kommentar zur DB-Sicherung speichern möchten, 
geben Sie diesen bitte jetzt ein. Falls nicht - Eingabetaste.
"
read kommentar

if [[ $kommentar != '' ]]
then
if [ -s Kommentare-DB-Sicherung.txt ]
then
echo -e "Sikop-DB--$neues_Verzeichnis-$datum.sql \n$kommentar\n\n" >> Kommentare-DB-Sicherung.txt
else
touch Kommentare-DB-Sicherung.txt
echo -e "Sikop-DB--$neues_Verzeichnis-$datum.sql \n$kommentar\n\n" > Kommentare-DB-Sicherung.txt
fi
fi

gzip Sikop-DB--$neues_Verzeichnis-$datum.sql

echo "
Der sql-Export liegt gezippt im Verzeichnis $pfadtry/$neues_Verzeichnis/Sikop für Sie bereit.

Ende des Skripts."


