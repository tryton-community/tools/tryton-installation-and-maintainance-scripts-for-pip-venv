
# Konfigurierbare Variablen
DB_HOST="localhost"  # Optional: Falls die DB auf einem anderen Host liegt
DB_PORT="5432" # Standard-Port für PostgreSQL

echo "Name der Datenbank:
"
read DB_NAME 

echo "DB-Benutzername:
"
read DB_USER

echo "DB-Kennwort:
"
read DB_KW


# Liste der zu leerenden Tabellen
TABLES=(
 account_asset_line
 account_invoice
 account_invoice_account_move_line
 account_invoice_additional_account_move  
 account_invoice_line 
 account_invoice_line_stock_move  
 account_invoice_line_account_tax 
 account_journal_period_history  
 account_journal_sequence 
 account_move
 account_move_line
 account_move_reconcile_write_off 
 account_move_reconciliation  
 account_payment_sepa_message 
 account_statement_line
 account_statement_origin 
 account_statement_origin_information
 project_work_invoiced_progress
 project_work_status  
 purchase_amendment_line  
 purchase_invoice_ignored_rel 
 purchase_invoice_recreated_rel
 purchase_purchase
 purchase_request
 sale_sale 
 sale_amendment
 sale_line
 sale_line_history
 sale_line_account_tax
 sale_line_account_tax_history
 sale_line_component  
 sale_line_component_ignored_stock_move
 sale_line_component_recreated_stock_move 
 sale_line_moves_ignored_rel  
 sale_line_moves_recreated_rel
 sale_sale_history
 stock_forecast_line  
 stock_inventory_line 
 stock_move
 timesheet_line
 )

# Bestätigung einholen
echo "
WARNUNG: 
Dieses Skript löscht alle Einträge 
aus den angegebenen Datenbank-Tabellen!
"
read -p "SIND SIE SICHER, DAẞ SIE DAS MÖCHTEN? (j/n): " CONFIRMATION
if [[ "$CONFIRMATION" != "j" ]]; then
echo "Abbruch."
exit 1
fi

read -p "WIKRLICH (ja/nein): " CONFIRMATION
if [[ "$CONFIRMATION" != "ja" ]]; then
echo "Abbruch."
exit 1
fi

# Löschen der Tabelleneinträge
for TABLE in ${TABLES[@]}; do
echo "Lösche alle Einträge aus $TABLE..."
PSQL_CMD="TRUNCATE TABLE $TABLE RESTART IDENTITY CASCADE;"
PGPASSWORD=$DB_KW psql -h "$DB_HOST" -p "$DB_PORT" -U "$DB_USER" -d "$DB_NAME" -c "$PSQL_CMD"
if [[ $? -eq 0 ]]; then
echo "Tabelle $TABLE erfolgreich geleert."
else
echo "Fehler beim Leeren der Tabelle $TABLE."
fi
done

echo "Alle angegebenen Tabellen wurden geleert."

