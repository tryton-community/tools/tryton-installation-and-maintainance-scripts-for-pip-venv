#! /bin/bash

# INSTALLATIONSSKRIPT ZUR INSTALLATION ZUSÄTZLICHER MODULE IN TRYTON

# set -x
# killall trytond
# killall trytond-cron
datum=`date +%d.%m.%Y`

echo "
Wir gehen davon aus, daß die virtuellen Umgebungen 
für Ihre Tryton-Versionen ~/TRYTON liegen sollen oder bereits liegen. 

In diesem Fall: Eingabetaste.

Sofern Sie einen anderen Pfad in Ihrem Heimatverzeichnis wünschen,
geben Sie diesen bitte OHNE "/home/ihr-Benutzername/" ein.
"
read Pfadkennung
if [ "$Pfadkennung" = "" ]
then
mkdir ~/TRYTON
pfadtry=~/TRYTON
else
mkdir ~/$Pfadkennung
pfadtry=~/$Pfadkennung
fi

cd $pfadtry

echo "Haben das Verzeichnis der virtuellen Umgebung 
und die Datenbank den gleichen Namen (j/n)?
"
read gl_Name

if [ "$gl_Name" = "n" ]
then
echo "Wie heißt das Verzeichnis der virtuellen Umgebung?
"
read neues_Verzeichnis

echo "Wie heißt die Datenbank?
"
read neue_db_Version

else
echo "Wie heißen Verzeichnis und Datenbank?
"
read neues_Verzeichnis
neue_db_Version=$neues_Verzeichnis
fi

cd $neues_Verzeichnis
. bin/activate
pip install --upgrade pip

pip freeze --local > installiert_am_$datum.txt


echo "Welche Module möchten Sie installieren?
Geben Sie sie im Format 'trytond-[modul-name]' an, 
ohne die Anführungszeichen, mit Bindestrichen oder Unterstrichen. 
Mehrere Module durch ein Leerzeichen trennen. 

"
read zuinstallieren

zuinstallieren="${zuinstallieren/_/-}"

echo "Welche Version möchten Sie installieren?
Bitte im Format 'x.y.*' eingeben; 'a' installiert die aktuelle Version."
read gew_Versionsnummer

#if [ "$gew_Versionsnummer" = "a" ]
#then
pip install $zuinstallieren==$gew_Versionsnummer
#else
#echo $zuinstallieren > mod_zu_installieren.lst

#sed -i "s/$/==$gew_Versionsnummer/g" mod_zu_installieren.lst
#sed -i "s/ /==$gew_Versionsnummer \n/g" mod_zu_installieren.lst

#pip install -r mod_zu_installieren.lst
#rm mod_zu_installieren.lst

#fi

# Benutzerdaten für privates Pypi einlesen:
#IFS=";"
#read -a BenDaten < *.prpy
#BenPrpy=${BenDaten[0]}
#KennwPrpy=${BenDaten[1]}
#ServerPrpy=${BenDaten[2]}


#sed -i "s/$/==$gew_Versionsnummer/g" *.lstprpy
#sed -i "s/ /==$gew_Versionsnummer \n/g" *.lstprpy


# Module aus privaten Pypi installieren:
#pip install --no-index --find-links https://$BenPrpi:$KennwPrpy@$ServerPrpy -r *.lstprpy
#

pip freeze --local > installiert_am_$datum.lst


echo "Der folgende Schritt kann mehrere Minuten dauern, bitte Geduld! (Weiter mit Eingabetaste)."
read

zuregistrieren="""${zuinstallieren#*-}" # entfernt "trytond, mds" etc vor Modulname
zuregistrieren="${zuregistrieren/-/_}" # ersetzt - durch _

trytond-admin -c trytond.conf -d $neues_Verzeichnis -u $zuregistrieren -vv

echo "
Möchten Sie den Tryton-Server automatisch starten ?
(Konsole, nicht als Service)
"
read starten
if [ "$starten" = "j" ]
then
trytond -c trytond.conf -d $neues_Verzeichnis &
trytond-cron -c trytond.conf -d $neues_Verzeichnis &

echo "
Der Tryton-Server wurde gestartet."
fi

echo "

Ende des Skripts - wir wünschen noch einen schönen Tag."


