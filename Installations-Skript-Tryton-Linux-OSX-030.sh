#! /bin/bash

clear

# INSTALLATIONSSKRIPT ZUR NEUINSTALLATION VON TRYTON


	
echo "
INSTALLATIONSSKRIPT ZUR NEUINSTALLATION VON TRYTON.
"
Benutzer=$(whoami)
instskrverz=$(pwd)

## Auf alte eingabe.conf überprüfen

if [ -e "eingabe.conf" ]; then
clear
echo "
Im Verzeichnis $instskrverz ist eine Datei mit älteren Eingaben (evt. aus vorangegangenen Installationsversuchen) mit folgendem Inhalt vorhanden:
"

cat $instskrverz/eingabe.conf 

echo "
Bitte korrigieren Sie diese gegebenenfalls in einem anderen Terminal.
Möchten Sie diese Datei verwenden? (j/n)
"
read janein
  if [ $janein == "j" ]; then
    source ./eingabe.conf
  fi
fi

### ---


### Test auf sudo-Rechte ###

if [ -z "$sudo_getestet" ] # -z: vorhanden? - für Variablen
then
sudo -v
if [ $? -eq 0 ]
then
  echo "export sudo_getestet=ok" > eingabe.conf
else
echo "
Bitte verschaffen Sie (auf einem anderen Terminal) dem Benutzer $Benutzer sudo-Rechte mit dem Befehl
sudo usermod -aG sudo $Benutzer
(Weiter mit Eingabetaste.)
"
read
fi # $? -eq 0
fi # -z "$sudo_getestet"

# set -e # Ende nach 1.Fehler
# set -x # Fehlermeldungen ausgeben

instskrverz=$(pwd) # instskrverz ist das Verzeichnis, in denen das Installationsskript liegt
datum=`date +%d.%m.%Y`

# cd ~


### Hauptpfad für Tryton-Instanzen ###

if [ -z "$Pfad_TRYTON_Verz" ]
then

echo "
Wir gehen davon aus, daß die virtuellen Umgebungen für Ihre Tryton-Versionen ~/TRYTON liegen sollen oder bereits liegen. In diesem Fall: Eingabetaste.

Sofern Sie einen anderen Pfad in Ihrem Heimatverzeichnis wünschen, geben Sie diesen bitte OHNE "/home/ihr-Benutzername/" ein.
"
read Name_TRYTON_Verz
if [ "$Name_TRYTON_Verz" = "" ]
then
Name_TRYTON_Verz=TRYTON
fi
  if ! [ -d "$HOME/$Name_TRYTON_Verz" ] # -d: Prüfung auf Verzeichnis vorhanden
  then
    mkdir $HOME/$Name_TRYTON_Verz
    if [ $? -eq 0 ]
    then
      echo "$HOME/$Name_TRYTON_Verz angelegt."
    else
      exit 1
    fi
  fi
Pfad_TRYTON_Verz=$HOME/$Name_TRYTON_Verz

echo "export Pfad_TRYTON_Verz=$Pfad_TRYTON_Verz" >> $instskrverz/eingabe.conf
fi # Verweis auf [ -z "$Pfad_TRYTON_Verz" ] - Zeile 43

echo "Hauptpfad für Tryton: $Pfad_TRYTON_Verz"
# cd $Pfad_TRYTON_Verz


### Tryton-Version festlegen ###

if [ -z "$Versionsnummer" ]
then

echo "
Bitte geben Sie die gewünschte Versionsnummer im Format x.y (6.6, 7.0 o.ä.) ein.

Wenn Sie eine unserer Musterdatenbanken verwenden wollen, müssen Sie die Version 7.0 wählen.) 
"
read Versionsnummer

fi # [ -z "$Versionsnummer" ]

echo "Versionsnummer für Tryton: $Versionsnummer"
Zielversionsnummer===$Versionsnummer.* # Format " ==x.y.* "


### Verzeichnis und Datenbank der Instanz festlegen ###

if [ -z "$neues_Verzeichnis" ] || [ -z "$neue_db_Version" ] # || "oder"-Abfrage
then
echo "
Sollen Verzeichnis und Datenbank den gleichen Namen haben? "
while true
do
  read -p "(j/n): " gl_Name # Ausgabej/n; frage gl_Name ab
  if [ "$gl_Name" = "j" ] || [ "$gl_Name" = "n" ] # überprüfe, ob j oder n eingegeben
  then
    break # Sprung hinter "done"
  fi
done

if [ "$gl_Name" = "n" ]
then
echo "
Wie soll das neue Verzeichnis für die Installation der ersten virtuellen Umgebung für Tryton heißen?
"
read neues_Verzeichnis

echo "
Wie soll die neue Datenbank heißen?
"
read neue_db_Version

else

echo "
Wie sollen das neue Verzeichnis und die neue Datenbank heißen?
"
read neues_Verzeichnis
neue_db_Version=$neues_Verzeichnis

fi # [ "$gl_Name" = "n" ]

echo "export neues_Verzeichnis=$neues_Verzeichnis" >> $instskrverz/eingabe.conf
echo "export neue_db_Version=$neue_db_Version" >> $instskrverz/eingabe.conf

fi # [ -z "neues_Verzeichnis" ] || [ -z "neue_db_Version" ]


### Verzeichnis für die Instanz anlegen ###

if ! [ -d "$Pfad_TRYTON_Verz/$neues_Verzeichnis" ]
then
  mkdir $Pfad_TRYTON_Verz/$neues_Verzeichnis
  if [ $? -eq 0 ]
  then
    echo "$Pfad_TRYTON_Verz/$neues_Verzeichnis angelegt."
  else
    echo "Anlegen des Verzeichnisses $Pfad_TRYTON_Verz/$neues_Verzeichnis ist gescheitert. Bitte auf anderem Terminal korrigieren."
  read
  fi
fi

echo "Verzeichnis der Instanz: $Pfad_TRYTON_Verz/$neues_Verzeichnis"

cd $Pfad_TRYTON_Verz/$neues_Verzeichnis

### ---

### Datenbank-Benutzer und Datenbank-Kennwort abfragen ###

if [ -z "$db_Benutzer" ] || [ -z "$kw_dbBenutzer" ]
then
echo "
Sollen der System- und der Datenbank-Benutzer identisch sein ?
"
while true
do
  read -p "(j/n): " janein
  if [ "$janein" = "j" ] || [ "$janein" = "n" ]
  then
    break
  fi
done

if [ "$janein" = "j" ]
then
db_Benutzer=$Benutzer

else
echo "
Bitte geben sie den gewünschten Namen des Datenbank-Benutzers ein:"
read db_Benutzer
fi
echo "
Bitte geben Sie das bzw. ein neues Kennwort für Ihren Datenbank-Benutzer $db_Benutzer ein:"
read kw_dbBenutzer

echo "export db_Benutzer=$db_Benutzer" >> $instskrverz/eingabe.conf
echo "export kw_dbBenutzer=$kw_dbBenutzer" >> $instskrverz/eingabe.conf

fi # [ -z "$db_Benutzer" ] || [ -z "$kw_dbBenutzer" ]

echo "Datenbank-Benutzer: $db_Benutzer"

### Betriebssystem ermitteln ###

system=$(uname)
if ! [ "$system" = "Darwin" ]
then

## lsb_release prüfen:
lsb_release -a
if [[ $? -eq 1 ]]
then 
echo "
Bitte installieren Sie das Programm 'lsb-release'.
(Weiter mit Eingabetaste.)
"
read
fi

fi # ! [ "$system" = "Darwin" ]


### Distro-spezifische Installationen ###

if ! [ "$system" = "Darwin" ]
then
Distribution=$(lsb_release -is)
else
Distribution=Darwin
# Abfrage ob MacPorts installiert ist.
type port > /dev/null
if [ $? eq 1]; then
echo "
Bitte installieren Sie MacPorts.
(Weiter mit Eingabetaste)."
read
fi

fi
echo "Distribution: $Distribution"

if [ "$Distribution" = "Ubuntu" ] || [ "$Distribution" = "Debian" ]
then 
echo Ubuntu oder Debian 
sudo apt-get install sudo curl p7zip-full python3-venv python3-setuptools python3-dateutil python3-gi-cairo python3-gi-cairo gir1.2-goocanvas-2.0 postgresql
fi

if [ "$Distribution" = "ManjaroLinux" ] || [ "$Distribution" = "ArchLinux" ]
then
echo Manjaro oder Arch
sudo pacman -S --needed curl p7zip-full python python3-venv python3-setuptools python3-gi-cairo python3-gi-cairo gir1.2-goocanvas-2.0 goocanvas postgresql
fi

if [ "$Distribution" = "RedHat" ]
then
echo RedHat
fi

if [ "$Distribution" = "Darwin" ]
then
  # 7zip
  installiert=$(port installed | grep -o 7zip)
  if [ "$installiert" = "7zip" ]
  then
    echo "7zip ist installiert"
  else
    echo "Installiere 7zip:"
    sudo port install 7zip
  fi
  
  # python312
  # Suchbegriff taucht auch in "xorg-libxcb @1.16.1_0+python312" auf,
  # daher wird explizit auf die zwei Leerzeichen am Zeilenanfang getestet
  installiert=$(port installed | grep -o '^  python312')
  if [ "$installiert" = "  python312" ]
  then
    echo "python312 ist installiert"
  else
    echo "Installiere python312:"
    sudo port install python312
  fi
  
  # py312-pip (py312-setuptools ist in den Abhängigkeiten enthalten)
  installiert=$(port installed | grep -o py312-pip)
  if [ "$installiert" = "py312-pip" ]
  then
    echo "py312-pip ist installiert"
  else
    echo "Installiere py312-pip:"
    sudo port install py312-pip
  fi
  
  # py312-dateutil
  installiert=$(port installed | grep -o py312-dateutil)
  if [ "$installiert" = "py312-dateutil" ]
  then
    echo "py312-dateutil ist installiert"
  else
    echo "Installiere py312-dateutil:"
    sudo port install py312-dateutil
  fi
  
  # py312-cairo
  installiert=$(port installed | grep -o py312-cairo)
  if [ "$installiert" = "py312-cairo" ]
  then
    echo "py312-cairo ist installiert"
  else
    echo "Installiere py312-cairo:"
    sudo port install py312-cairo
  fi
  
  # curl
  installiert=$(port installed | grep -o curl)
  if [ "$installiert" = "curl" ]
  then
    echo "curl ist installiert"
  else
    echo "Installiere curl:"
    sudo port install curl
  fi
  
  # postgresql13-server (postgresql13 ist in den Abhängigkeiten definiert)
  installiert=$(port installed | grep -o postgresql13-server)
  if [ "$installiert" = "postgresql13-server" ]
  then
    echo "postgresql13-server ist installiert"
  else
    echo "Installiere postgresql13-server:"
    sudo port install postgresql13-server
    export PATH="/opt/local/lib/postgresql13/bin:$PATH"
    echo "# Suchpfade für postgresql-Programme erweitern:" >> $instskrverz/eingabe.conf
    echo "export PATH=\"/opt/local/lib/postgresql13/bin:\$PATH\"" >> $instskrverz/eingabe.conf
  fi
  
  # Auwahl des Klienten

  if [ -z "$klient" ]
  then
    echo "Auswahl der Anwendung zum Arbeiten mit Tryton:"
    echo "- Desktop: benötigt X11-Server und GTK3"
    echo "- Web-Broweser: benötigt nodejs"
    PS3="Bitte Anwendung auswählen: "
    select klient in Desktop Web
    do
      if ! [ -z "$klient" ]
      then
        break
      fi
    done
    echo "export klient=$klient" >> eingabe.conf
  fi
  
  # Pakete, die für den Desktop-Client nötig sind
  if [ "$klient" = "Desktop" ]
  then
    # gobject-introspection
    installiert=$(port installed | grep -o gobject-introspection)
    if [ "$installiert" = "gobject-introspection" ]
    then
      echo "gobject-introspection ist installiert"
    else
      echo "Installiere gobject-introspection:"
      sudo port install gobject-introspection
    fi
  
    # xorg-server
    installiert=$(port installed | grep -o xorg-server)
    if [ "$installiert" = "xorg-server" ]
    then
      echo "xorg-server ist installiert"
    else
      echo "Installiere xorg-server:"
      sudo port install xorg-server
    fi
  
    # gtk3
    installiert=$(port installed | grep -o gtk3)
    if [ "$installiert" = "gtk3" ]
    then
      echo "gtk3 ist installiert"
    else
      echo "Installiere gtk3:"
      sudo port install gtk3
    fi
    
    echo "Melden Sie sich nach der Installation ab und wieder neu an,"
    echo "damit der X11-Server verfügbar ist."
  fi # [ "$klient" = "Desktop" ]
  
  # Pakete, die für den Web-Client nötig sind
  if [ "$klient" = "Web" ]
  then
    # nodejs20
    installiert=$(port installed | grep -o nodejs20)
    if [ "$installiert" = "nodejs20" ]
    then
      echo "nodejs20 ist installiert"
    else
      echo "Installiere nodejs20:"
      sudo port install nodejs20
    fi
  fi # [ "$klient" = "Web" ]
  
  echo "Benutzte Anwendung: $klient"
    
fi # [ "$Distribution" = "Darwin" ]

### Ende Distro-spezifische Installationen ###

## postgres aktivieren, falls noch nicht aktiv:

# (auf Mac OS X überspringen)
if ! [ "$system" = "Darwin" ]
then

sudo systemctl status postgresql.service | grep 'Active: active'
if [[ $? -eq 1 ]]
then 
sudo systemctl start postgresql
sudo systemctl enable --now postgresql.service
fi

fi # ! [ "$system" = "Darwin" ]


## UTF8-Codierung der Datenbank überprüfen
sudo --login -u postgres psql --command="\l" | grep 'UTF8' | grep 'template1' > /dev/null
if [[ $? -eq 1 ]]
then
echo "
Bitte überprüfen Sie, ob Ihre Datenbank-Vorlage UTF8-codiert ist. Falls nein, korrigieren Sie das und überprüfen auch die locale Ihres Systems.
"
fi
### Ende UTF8

## Auf systemweite Tryton- oder mds-Komponenten überprüfen 

pip list | grep -E 'tryton*|mds' > /dev/null
if [[ $? -eq 0 ]]
then
echo "
In Ihrem System sind evt. systemweit Tryton- oder MDS-Module installiert.
Bitte entfernen Sie diese mit 
pip uninstall <Modulname>.
" 
read
fi
###

## Laufende Tryton-Instanzen beenden

# (auf Mac OS X überspringen)
if ! [ "$system" = "Darwin" ]
then

echo "
Sollen aktive Tryton-Instanzen beendet werden ? (nicht auf Servern)
(ja=Eingabe)
"
read janein

if [ "$janein" = "" ] || [ "$janein" = "j" ]
then
killall trytond trytond-cron
# sudo systemctl restart postgresql* 
fi 
##

fi # ! [ "$system" = "Darwin" ]

### PIP und Tryton-Basis-Pakete installieren ###
echo "
PIP und Tryton-Basis-Pakete installieren:
"
python3 -m venv --system-site-packages $Pfad_TRYTON_Verz/$neues_Verzeichnis
cd $Pfad_TRYTON_Verz/$neues_Verzeichnis
. bin/activate
pip install --upgrade pip
if ! [ "$system" = "Darwin" ]
then
  pip install trytond$Zielversionsnummer proteus$Zielversionsnummer tryton$Zielversionsnummer
else
  pip install trytond$Zielversionsnummer proteus$Zielversionsnummer
  if [ "$klient" = "Desktop" ]
  then
    pip install tryton$Zielversionsnummer
  fi
fi # ! [ "$system" = "Darwin" ]
pip install bcrypt psycopg2-binary Genshi lxml passlib pycountry forex_python polib python-magic python-sql relatorio simplejson wrapt

if [ "$Versionsnummer" = "4.6" ] || [ "$Versionsnummer" = "6.0" ]
then
  pip install werkzeug==2.3.6
else
  pip install werkzeug
fi

### Ende PIP und Tryton-Basis-Pakete installieren ###


## MuDaBa und ModListen auswählen ##
clear
PS3="
Bitte wählen Sie eine unserer vorkonfigurierten Musterdatenbanken mit zugehöriger Modulliste aus:
"
select Paketauswahl in "Keine Musterdatenbank und Modulliste." "nur Buchhaltung SKR04 - v7.0" "Buchhaltung SKR04, Wareneinkauf, Warenverkauf - v7.0" "Buchhaltung SKR04, Wareneinkauf, Warenverkauf, Projektabrechnung - v7.0" "Nur Buchhaltung SKR03 - v7.0"
do
  if ! [ -z "$Paketauswahl" ]
  then
    break
  fi
done

echo "
Ausgewähltes Paket: $Paketauswahl
"

# In OS X wird curl anstatt wget verwendet
case $REPLY in
  1)
    echo "Es werden keine Musterdatenbanken/Modullisten heruntergeladen. Bitte legen Sie diese gegebenenfalls im nächsten Schritt in das VENV."
    # Paketauswahl auf null setzen, damit im folgenden nach einer manuellen Ablage gefragt wird. 
    Paketauswahl=0
    ;;
  2)
    curl -o "01-70-skr04-nur-Buha.zip" "https://foss.heptapod.net/tryton-community/tools/musterdatenbanken-de-germany/-/raw/main/01-70-skr04-nur-Buha.zip"
    ;;
  3)
    curl -o "02-70-skr04-Ein-Verkauf.zip" "https://foss.heptapod.net/tryton-community/tools/musterdatenbanken-de-germany/-/raw/main/02-70-skr04-Ein-Verkauf.zip"
        ;;
  4)
    curl -o "03-70-skr04-Ein-Verkauf-Projekt.zip" "https://foss.heptapod.net/tryton-community/tools/musterdatenbanken-de-germany/-/raw/main/03-70-skr04-Ein-Verkauf-Projekt.zip"
    ;;
  5)
    curl -o "10-70-skr03-nur-Buha.zip" "https://foss.heptapod.net/tryton-community/tools/musterdatenbanken-de-germany/-/raw/main/10-70-skr03-nur-Buha.zip"
    ;;
esac

### ZIP-Dateien entpacken, falls vorhanden ###

echo "
ZIP-Dateien entpacken. Dabei bitte die Fehlermeldung ignorieren,
falls keine ZIP-Dateien im Verzeichnis liegen.
"
7z e *.zip


cd $Pfad_TRYTON_Verz/$neues_Verzeichnis

## ModListen versionieren:

if ! [ "$Distribution" = "Darwin" ]
then

# gegebenenfalls Versionsnummer aus vorherigem Installationsversuch entfernen:
sed -i "s/==.*$//g" *.lst* 
## Leerzeilen entfernen aus allen Listen:
sed -i '/^$/d' *.lst*
# Zielversionsnummer einfügen:
sed -i -E '/^-|^#/!s/$'"/$Zielversionsnummer/g" *.lst

else

# OS X brauch bei sed -i einen Leerstring als Parameter

# gegebenenfalls Versionsnummer aus vorherigem Installationsversuch entfernen:
sed -i '' "s/==.*$//g" *.lst* 
# Leerzeilen entfernen aus allen Listen:
sed -i '' '/^$/d' *.lst*
# Leerzeichen entfernen aus allen Listen:
sed -i '' 's/ \{1,\}//g' *.lst*
# Zielversionsnummer einfügen:
sed -i '' -E '/^-|^#/!s/$'"/$Zielversionsnummer/g" *.lst

fi

# Benutzerdaten aus Datei holen

if [ -e "Benutzerdaten.dat" ]
then
source Benutzerdaten.dat
echo $PI_BENUTZER $PI_KENNWORT
export PI_BENUTZER PI_KENNWORT
fi

pip install -r 01Inst*


echo "
Installieren Sie gegebenenfalls manuell zu installierende Pakete - zum Beispiel von github oder aus .whl-Dateien - in einem separaten Terminal-Fenster. Die entsprechenden Befehle lauten normalerweise

cd $Pfad_TRYTON_Verz/$neues_Verzeichnis
. bin/activate
pip install --no-dependencies [Name-desPakets].whl

(Eingabetaste, wenn fertig.)
"
read

sudo --login -u postgres psql --command="\du" | grep $db_Benutzer > /dev/null
if [[ $? -ne 0 ]]
then
sudo --login -u postgres psql --command="CREATE ROLE $db_Benutzer WITH PASSWORD '$kw_dbBenutzer';"
sudo --login -u postgres psql --command="ALTER ROLE $db_Benutzer WITH LOGIN;"
fi

### prüfen, ob neue DaBa schon vorhanden ist:

psql -U $db_Benutzer -l | grep $neues_Verzeichnis > /dev/null
if [ $? -eq 0 ]; then
sudo --login -u postgres dropdb $neue_db_Version
fi
sudo --login -u postgres createdb -O $db_Benutzer $neue_db_Version
if [[ $? -ne 0 ]]
then
echo "Erzeugung der Datenbank gescheitert - bitte überprüfen! (Weiter mit Eingabetaste)"
read
fi



cd $Pfad_TRYTON_Verz/$neues_Verzeichnis

if [ -s *.sql ]; then
# Danke, EdBo !
psql -U $db_Benutzer -d $neues_Verzeichnis -h localhost  < *.sql
if [[ $? -ne 0 ]]
then
echo "Import des Datenbank-Dumps gescheitert - bitte überprüfen! (Weiter mit Eingabetaste)"
read
fi
fi

## trytond.conf erzeugen

echo "[database]
uri = postgresql://$db_Benutzer:$kw_dbBenutzer@localhost:5432/

language = de

"  > $Pfad_TRYTON_Verz/$neues_Verzeichnis/trytond.conf 

### Datenbank in venv registrieren ###

echo "
Der folgende Schritt dauert einige Minuten.
"
trytond-admin -c trytond.conf -d $neue_db_Version --all -vv
### Ende Datenbank in venv registrieren ###

if [ "$Versionsnummer" = "6.0" ]
then
pip install werkzeug==2.3.6
fi

echo "
Wenn Sie eine weitere Sprache außer Englisch wünschen, geben Sie bitte deren beiden Kennbuchstaben (fr, es, de, it etc.) ein.
"
read sprache
trytond-admin -c trytond.conf -d $neue_db_Version -l $sprache

echo "
Falls Sie neben Englisch eine weitere Sprache installiert haben, müssen Sie diese in der graphischen Benutzeroberfläche aktivieren:

Menü > Administration > Localisation > Sprachen ==> load translation.
"

### Startskript erzeugen ###
echo "
Möchten Sie eine Startskript für Ihre Installation erzeugen (j/n)?
"
read janein

if [ "$janein" = "j" ]
then

if ! [ "$Distribution" = "Darwin" ]
then

echo "#!/bin/bash

# Startskript für Tryton
# Es wird erwartet, daß Datenbank und VirtEnv den gleichen Namen haben und der passende Klient installiert ist.

Tryversion=$neues_Verzeichnis # Name Ihrer virtuellen Umgebung
TryPfad=$Pfad_TRYTON_Verz/$neues_Verzeichnis/ # Pfad, in der Ihre virtuellen Umgebungen liegt

killall trytond 
killall trytond-cron
cd \$TryPfad/
. bin/activate
sleep .5
trytond-cron -c trytond.conf -d \$Tryversion &
trytond -c trytond.conf -d \$Tryversion &
sleep 2
tryton" > $Pfad_TRYTON_Verz/$neues_Verzeichnis/Startskript-$neues_Verzeichnis.sh

else

echo "#!/bin/bash

# Startskript für Tryton
# Es wird erwartet, daß Datenbank und VirtEnv den gleichen Namen haben und der passende Klient installiert ist.

Tryversion=$neues_Verzeichnis # Name Ihrer virtuellen Umgebung
TryPfad=$Pfad_TRYTON_Verz/$neues_Verzeichnis/ # Pfad, in der Ihre virtuellen Umgebungen liegt

cd \$TryPfad/
source bin/activate
sleep .5
trytond -c trytond.conf -d \$Tryversion 2> trytond.log &
" > $Pfad_TRYTON_Verz/$neues_Verzeichnis/Startskript-$neues_Verzeichnis.sh
if [ $klient = "Desktop" ]
then
echo "sleep 2
tryton 2> tryton.log &" >> $Pfad_TRYTON_Verz/$neues_Verzeichnis/Startskript-$neues_Verzeichnis.sh
fi

fi # ! [ "$Distribution" = "Darwin" ]

chmod +x Startskript-$neues_Verzeichnis.sh

echo "
Das Startskript liegt im Verzeichnis $TryPfad/$neues_Verzeichnis unter dem Namen 'Startskript-$neues_Verzeichnis.sh' für Sie bereit.
"
fi
### Ende Startskript erzeugen ###

# /bin/bash

### Sicherungsskript erzeugen ###

echo "
Möchten Sie ein Sicherungsskript erzeugen (j/n) ?
"
read janein
if [ "$janein" = "j" ]; then
echo "
#! /bin/bash


alteNr=1

### Hier Benutzereingaben: ###
sikopPfad=$Pfad_TRYTON_Verz/$neues_Verzeichnis/Sikop # Pfad der Sicherungskopien !
dbName=$neues_Verzeichnis # Name Datenbank 
dbBenutzer=$db_Benutzer # Datenbank-Benutzername 
skriptPfad=$Pfad_TRYTON_Verz/$neues_Verzeichnis/ # Pfad dies Skripts !
nameSkript=Sicherungsskript-$neues_Verzeichnis.sh # Name dieses Skripts !
db_Kennwort=$kw_dbBenutzer # Datenbank-Kennwort !
### Ende Benutzereingaben. ###

WoTag=\`date +%a\`
neueNr=\$(( \$alteNr+1 ))
datum=\`date +%d.%m.%Y\`
nameSikopNeu=\$dbName--\$datum-\$WoTag--\$neueNr.sql

PGPASSWORD=\$db_Kennwort pg_dump -U \$dbBenutzer \$dbName > \$sikopPfad/\$nameSikopNeu

# echo \$sikopPfad/\$dbName*--\$neueNr.sql \$sikopPfad/\$dbName*--\$alteNr.sql

cmp \$sikopPfad/\$dbName*--\$neueNr.sql \$sikopPfad/\$dbName*--\$alteNr.sql
 if [ \$? == 0 ]; then
	rm \$sikopPfad/\$dbName*--\$neueNr.sql
	neueNr=\$(( \$neueNr-1 ))
	echo gleich
  else
	7z a \$sikopPfad/\$dbName-\$datum--\$alteNr.7z \$sikopPfad/\$dbName*--\$alteNr.sql 
	# echo verschieden
	sed -i "5i alteNr=\${neueNr}" \$skriptPfad/\$nameSkript
	sed -i '6d' \$skriptPfad/\$nameSkript
 fi
" > $Pfad_TRYTON_Verz/$neues_Verzeichnis/Sicherungsskript-$neues_Verzeichnis.sh
fi

sudo chmod +x $Pfad_TRYTON_Verz/$neues_Verzeichnis/Sicherungsskript-$neues_Verzeichnis.sh

mkdir $Pfad_TRYTON_Verz/$neues_Verzeichnis/Sikop

clear

echo "
Das Sicherungsskript liegt im Verzeichnis $Pfad_TRYTON_Verz/$neues_Verzeichnis für Sie bereit.
Um es automatisch zu starten:

Bei 24/7 laufenden Servern erzeugen Sie mit crontab -e einen cronjob, zum Beispiel:
0       2       *       *       *       /bin/bash -c "$Pfad_TRYTON_Verz/Skripte/$nameSkript"
Dieser Aufruf startet das Skript täglich um 02.00h.

Bei Arbeitsplatzrechnern können Sie das Skript über die 'Startobjekte' bei jedem Start des Rechners automatisch aufrufen.
"
### Ende Sicherungsskript erzeugen ###

### Zusammenfassung erzeugen ###

touch $Pfad_TRYTON_Verz/$neues_Verzeichnis/Installationsdaten-$neues_Verzeichnis.txt

echo "
installiert am: $datum
installierte Tryton-Version: $Versionsnummer
Datenbank-Name: $neue_db_Version

Pfad der Installation: $Pfad_TRYTON_Verz
Verzeichnis der Installation: $Pfad_TRYTON_Verz/$neues_Verzeichnis

Datenbank-Benutzername: $db_Benutzer
Kennwort des Datenbank-Benutzers: $kw_dbBenutzer


" > $Pfad_TRYTON_Verz/$neues_Verzeichnis/Installationsdaten-$neues_Verzeichnis.txt


echo " 
Geben Sie ein Passwort für den Administrator im Tryton-Klient ein, aber erst, wenn die Abfrage

admin password for $neues_Verzeichnis

auftaucht.
"

trytond-admin -c trytond.conf -d $neue_db_Version -p 

echo "Verwenden Sie zum Login in Tryton den Benutzernamen "admin" mit dem soeben erzeugten Passwort. 
(Weiter mit Eingabetaste)"
read

echo "
Die Installation von Tryton für einen Einzelplatzrechner ist damit abgeschlossen. Unter $Pfad_TRYTON_Verz/$neues_Verzeichnis finden Sie die Datei Installationsdaten-$neues_Verzeichnis.txt, die alle Angaben zu dieser Installation enthält.
"

### Währungen etc. importieren
echo "
Möchten Sie Weltwährungen, Postleitzahlen oder Länder in Ihre Installation importieren? (j/n)
"
read janein
if [ "$janein" = "j" ]; then
clear

echo "#! /bin/bash
trytond_import_countries -c trytond.conf -d $neues_Verzeichnis
trytond_import_currencies -c trytond.conf -d $neues_Verzeichnis
trytond_import_postal_codes -c trytond.conf -d $neues_Verzeichnis $sprache
" > Waehrung-Laender-PLZ-Import-0001.sh

chmod +x Waehrung-Laender-PLZ-Import-0001.sh

echo "
Bitte loggen Sie sich nach Abschluß der Installation in Ihr Tryton ein und aktivieren Sie alle Module.

Danach aktivieren Sie bitte Ihr VENV und führen das Skript 'Waehrung-Laender-PLZ-Import-0001.sh' aus, das wir dort bereitgestellt haben.
"
fi

echo "
Wenn Sie Tryton als Server (egal ob im lokalen Netz oder entfernt) einrichten möchten, drücken Sie bitte jetzt 'j'
"
read janein
if ! [ "$janein" = "j" ]; then

echo "
Ende des Skripts. Rückmeldung bitte an wd@trolink.de.
"
exit 0
fi

# Abschnitt Webserver-Installationen

### Trytond als systemd einrichten. ###

sudo echo "
[Unit]
Description=$neues_Verzeichnis starten
After=network.target
After=postgresql.service
StartLimitIntervalSec=5

[Service]
Type=simple
RestartSec=5
User=$Benutzer
ExecStartPre= /usr/bin/bash -c 'cd $Pfad_TRYTON_Verz/$neues_Verzeichnis && source bin/activate'
ExecStart= $Pfad_TRYTON_Verz/$neues_Verzeichnis/bin/trytond -c $Pfad_TRYTON_Verz/$neues_Verzeichnis/trytond.conf -d $neues_Verzeichnis
Restart=always

[Install]
WantedBy=multi-user.target
" > /tmp/$neues_Verzeichnis.service

sudo mv /tmp/$neues_Verzeichnis.service /etc/systemd/system/

echo "
Tryton wurde als Service des systemd eingericht. Möchten Sie diesen Service jetzt aktivieren und starten (j/n) ?
"
read janein
if [ "$janein" = "j" ]; then

sudo systemctl enable $neues_Verzeichnis.service
sudo systemctl start $neues_Verzeichnis.service
fi

### Ende Trytond als systemd einrichten. ###


### Nginx installieren und starten ###

echo "
Möchten Sie den Webserver 'nginx' installieren (j/n)?
"
read janein
if [ "$janein" = "j" ]; then
sudo apt install nginx
fi

echo "
Auf welchem Port soll Ihre Tryton-Instanz laufen? Standardmäßig geben Sie bitte 8000 ein.
"

## Überprüfen, ob Port schon belegt.
read Portnummer

sudo netstat -tlp | grep $Portnummer
if [ "$?" == "0" ]; then
echo "Der Port $Portnummer scheint bereits belegt. Bitte geben Sie gegebenenfalls eine andere Portnummer ein."
read Portnummer
fi

#Portnummer=8000

cd /etc/nginx

echo "
Wie lautet Ihre (Sub-)Domain ? (ohne 'http', 'https' etc eingeben)
"
read Domainname

#Domainname=test.domain.de


sudo echo "
# GNU nano 6.2     /etc/nginx/sites-available/$neues_Verzeichnis
# nginx server configuration für:
# $Domainname

server {
		listen 80;
        listen [::]:80;
        server_name $Domainname;


  location / {
    proxy_set_header Host \$host;
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_pass http://127.0.0.1:$Portnummer;
  }
}  
" > /tmp/$neues_Verzeichnis

sudo mv /tmp/$neues_Verzeichnis /etc/nginx/sites-available/$neues_Verzeichnis
sudo rm /tmp/$neues_Verzeichnis
sudo ln -s /etc/nginx/sites-available/$neues_Verzeichnis /etc/nginx/sites-enabled/$neues_Verzeichnis

echo "
[web]
listen=127.0.0.1:$Portnummer
root=$Pfad_TRYTON_Verz/$neues_Verzeichnis/sao
" >> $Pfad_TRYTON_Verz/$neues_Verzeichnis/trytond.conf

## sao installieren ##
sudo apt install npm
cd $Pfad_TRYTON_Verz/$neues_Verzeichnis
curl -o tryton-sao-last.tgz "https://downloads.tryton.org/$Versionsnummer/tryton-sao-last.tgz"
tar -xzf tryton-sao-last.tgz
mv package sao
cd sao
npm install --production --legacy-peer-deps
rm ../tryton-sao-last.tgz

## Ende sao installieren ##
### Ende Nginx installieren und starten ###


### SSL-Zertifikate einrichten ###
echo "
Möchten Sie die SSL-gesicherte Datenübertragung per certbot einrichten (j/n)?
"
read janein
if [ "$janein" = "j" ]; then

echo "
Gegebenenfalls sollten Sie den Status Ihrer Firewall überprüfen, zum Beispiel mit:

sudo ufw status

(Weiter mit Eingabetaste).
"
read

sudo apt install certbot python3-certbot-nginx
sudo certbot --nginx -d $Domainname
fi

if [ -e /var/run/nginx.pid ]; then 
sudo systemctl restart nginx;
else
sudo systemctl enable nginx
sudo systemctl start nginx
fi

### Ende SSL-Zertifikate einrichten ###

## Zusammenfassung erzeugen

echo
"Bitte geben Sie eine Beschreibung der Installation für die Zusammenfassung ein.
"
read Beschreib

cd $Pfad_TRYTON_Verz
if ! [ -s = "Zusammenfassung.txt" ]
then
touch Zusammenfassung.txt
fi
echo "
Beschreibung: $Beschreib
Port: $Portnummer
Domainname: $Domainname
Datenbank: $neue_db_Version
Systemd-Service: $neues_Verzeichnis.service

" >> $Pfad_TRYTON_Verz/Zusammenfassung.txt


echo "
Ende des Skripts. Rückmeldung bitte an wd@trolink.de.
"

