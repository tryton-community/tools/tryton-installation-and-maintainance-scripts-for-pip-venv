#! /bin/bash

# killall tryton trytond trytond-cron

set +x

echo "
Dieses Skript importiert eine vorhandene Datenbank
in eine vorhandene Installation mit gleicher Hauptversionsnummer.
(Eingabe)
"
read


# set -x
instskrverz=$(pwd) # instskrverz ist das Verzeichnis, in denen das Installationsskript liegt

datum=`date +%d.%m.%Y`

cd ~

echo "
Wir gehen davon aus, daß die virtuellen Umgebungen für Ihre Tryton-Versionen
unter ~/TRYTON liegen sollen oder bereits liegen.
In diesem Fall: Eingabetaste.

Sofern Sie einen anderen Pfad in Ihrem Heimatverzeichnis wünschen, 
geben Sie diesen bitte OHNE "/home/ihr-Benutzername/" ein.
(Eingabe)
"
read Pfadkennung
if [ "$Pfadkennung" = "" ]
then
pfadtry=~/TRYTON
else
mkdir ~/$Pfadkennung
pfadtry=~/$Pfadkennung
fi

echo "
Wie heißt das Verzeichnis der virtuellen Umgebung, 
die zu der zu aktualisierenden Datenbank gehört?
"
read neues_Verzeichnis
neue_db_Version=$neues_Verzeichnis

echo "
Bitte geben sie den Namen des Datenbank-Benutzers ein:
"
read db_Benutzer

sudo ps -ef | grep $neues_Verzeichnis

echo "
Sofern der Name Ihrer Datenbank hier als 'verwendet' auftaucht,
beenden Sie bitte die entsprechende Tryton-Instanz.
(Eingabe)
" 

### prüfen, ob neue DaBa schon vorhanden ist:

psql -U $db_Benutzer -l | grep $neues_Verzeichnis > /dev/null
if [ $? -eq 0 ]; then
sudo --login -u postgres dropdb $neue_db_Version
fi
sudo --login -u postgres createdb -O $db_Benutzer $neue_db_Version
if [[ $? -ne 0 ]]
then
echo "
Import der Datenbank gescheitert - bitte überprüfen!
(Weiter mit Eingabetaste)
"
read
fi


echo "
Im folgenden Dialog:
Dateiauswahl mit Leertaste, Bestätigung mit '/'.
"

sleep 2

SIKOPDATEI=$(dialog --stdout --title "Datei auswählen" --fselect  $pfadtry 14 60)

case "$SIKOPDATEI" in
    *.zip|*.gzip|*.gz|*.7z) 
        7z e -so $SIKOPDATEI > /$pfadtry/$neues_Verzeichnis/sqldatei-$datum.sql ;; # 7z-Ausgabe in Datei umleiten
    *)  
        echo "Die Datei ist kein ZIP- oder GZIP-Archiv."
        cp -i $SIKOPDATEI /$pfadtry/$neues_Verzeichnis/sqldatei-$datum.sql ;;
esac

psql -U $db_Benutzer -d $neue_db_Version -h localhost < /$pfadtry/$neues_Verzeichnis/sqldatei-$datum.sql

echo "Der folgende Schritt kann einige Minuten dauern.

"
sleep 2

cd /$pfadtry/$neues_Verzeichnis
. bin/activate

trytond-admin -c trytond.conf -d $neue_db_Version --all -vv

echo "
Ende des Skripts - wir wünschen noch einen schönen Tag.
"
