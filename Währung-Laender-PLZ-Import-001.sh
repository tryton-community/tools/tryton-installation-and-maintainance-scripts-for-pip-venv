#! /bin/bash

clear

echo "
Dieses Skript importiert Länder, Währungen und Postleitzahlen IN EINE Tryton-Installation. 

Wir gehen davon aus, daß die virtuellen Umgebungen für Ihre Tryton-Versionen ~/TRYTON liegen sollen oder bereits liegen. In diesem Fall: Eingabetaste.

Sofern Sie einen anderen Pfad in Ihrem Heimatverzeichnis wünschen, geben Sie diesen bitte OHNE "/home/ihr-Benutzername/" ein.
"
read Pfadkennung
if [ "$Pfadkennung" = "" ]
then
pfadtry=~/TRYTON
else
pfadtry=~/$Pfadkennung
fi


echo "
Wie lautet der Name der Datenbank? 
(Er ist normalerweise mit dem Namen des VENV identisch.
"
read neue_db_Version
#basename ${PWD}=neue_db_Version

cd $pfadtry/$neue_db_Version
. bin/activate

### Länderliste ###
echo "
Möchten Sie eine Liste aller Länder weltweit importieren (j/n)?
"
read janein

if [ "$janein" = "j" ]
then
trytond_import_countries -d $neue_db_Version -c trytond.conf
fi
### Ende Länderliste ###

### Währungen ###
echo "
Möchten Sie eine Liste aller Weltwährungen importieren (j/n)?
"
read janein

if [ "$janein" = "j" ]
then
trytond_import_currencies -c trytond.conf -d $neue_db_Version 
fi
### Ende Währungen ###

### PLZs ###
echo "
Möchten Sie eine Liste aller Postleitzahlen Ihres Landes importieren - dauert mehrere Minuten (j/n)?
"
read janein

if [ "$janein" = "j" ]
then
echo "
Bitte geben Sie Ihren Ländercode ein (de, fr, it etc).
"
read sprache
trytond_import_postal_codes -c trytond.conf -d $neue_db_Version $sprache
fi
### Ende PLZs ###

clear

echo Ende des Skripts, Kommentare bitte an wd@trolink.de.
